## Requirements

- AWS Account
- GCP Account
- Terraform Version `Terraform v0.12.19`

## Setup
- Copy the google account.json in the terraform directory
- `terraform init`
- `terraform apply`

## Security Hardening
- Firewalld and SELinux (See ansible playbook for more details)

## Monitoring
- GCP Stackdriver as per the requirements mentioned in the test
- For logging the commands `auditd` is used.

## SSH
- AWS Systems Manager Sessions Manager


Also since systems manager plugin in installed we can rollout security patches easily and also use the inventory feature to get the list of packages that are installed. 
