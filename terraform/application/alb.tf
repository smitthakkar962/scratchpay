module "nginx-echo-alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"
  
  name = "nginx-echo"

  load_balancer_type = "application"

  vpc_id             = data.terraform_remote_state.network.outputs.vpc_id
  subnets            = data.terraform_remote_state.network.outputs.public_subnets
  security_groups    = [
      aws_security_group.alb-public.id
  ]

  target_groups = [
    {
      name_prefix      = "nginx"
      backend_protocol = "HTTP"
      backend_port     = 3333
      target_type      = "instance"
    }
  ]

  http_tcp_listeners = [
    {
      port               = 3333
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  tags = {
    Environment = "Scratch Pay"
  }
}
