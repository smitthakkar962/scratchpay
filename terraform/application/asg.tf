resource "aws_iam_role" "nginx_echo_iam_role" {
  name = "nginx-echo-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Terraform = "true"
    Environment = "scratch-pay"
  }
}

resource "aws_iam_instance_profile" "nginx_echo_iam_role" {
  name = "nginx-echo-instance-profile"
  role = aws_iam_role.nginx_echo_iam_role.name
}

resource "aws_iam_role_policy_attachment" "ssm-policy-attachment" {
  role       = aws_iam_role.nginx_echo_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"
  
  name = "nginx-echo"

  # Launch configuration
  lc_name = "nginx-echo-lc"

  image_id        = "ami-0ff760d16d9497662"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.alb-access.id]
  health_check_grace_period	= 480
  iam_instance_profile	 = aws_iam_instance_profile.nginx_echo_iam_role.name
  target_group_arns = module.nginx-echo-alb.target_group_arns
  root_block_device = [
    {
      volume_size = "8"
      volume_type = "gp2"
    },
  ]

  user_data = <<EOF
#!/usr/bin/env bash
sudo yum install -y awscli unzip
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo systemctl status amazon-ssm-agent
sudo systemctl enable amazon-ssm-agent
sudo systemctl start amazon-ssm-agent
sudo aws s3 cp s3://${aws_s3_bucket.nginx-echo-provisioner.bucket}/${aws_s3_bucket_object.object.key} ansible.zip
sudo unzip ansible.zip
sudo easy_install pip
sudo pip install ansible pushbullet.py
ansible-galaxy install -r requirements.yaml
ansible-playbook nginx.yaml &> /tmp/ansible-output.txt
  EOF

  # Auto scaling group
  asg_name                  = "nginx-echo-asg"
  vpc_zone_identifier       = data.terraform_remote_state.network.outputs.private_subnets
  health_check_type         = "EC2"
  min_size                  = 0
  max_size                  = 2
  desired_capacity          = 2
  wait_for_capacity_timeout = 0

  tags_as_map = {
        Terraform = "true"
        Environment = "scratch-pay"
    }
}
