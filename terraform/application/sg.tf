resource "aws_security_group" "alb-public" {
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  ingress {
    from_port   = 3333
    to_port     = 3333
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "alb-access" {
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  ingress {
    from_port   = 3333
    to_port     = 3333
    protocol    = "tcp"
    security_groups = [aws_security_group.alb-public.id]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
