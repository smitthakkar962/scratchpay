terraform {
  required_version = ">= 0.12.19"
}

provider "aws" {
  profile = "personal"
  region = "eu-west-1"
}

provider "google" {
  credentials = file("${path.module}/../account.json")
  project     = var.gcp_project_id
  region      = "europe-west1"
  version = "2.12.0"
}
