resource "google_service_account" "nginx-echo-access" {
  account_id   = "nginx-echo"
  display_name = "nginx-echo"
}

resource "google_project_iam_member" "logging-permission" {
  role    = "roles/logging.logWriter"
  member = "serviceAccount:${google_service_account.nginx-echo-access.email}"
}

resource "google_service_account_key" "aws-nginx-echo-instance" {
  service_account_id = google_service_account.nginx-echo-access.name
}

resource "aws_ssm_parameter" "secret" {
  name        = "gcp-stackdriver-serviceaccount"
  type        = "SecureString"
  value       = base64decode(google_service_account_key.aws-nginx-echo-instance.private_key)

  tags = {
    environment = "Scratch Pay"
  }
}

resource "aws_iam_role_policy" "gcp-serviceaccount-private-key-ssm-permission" {
  role = aws_iam_role.nginx_echo_iam_role.name
  policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": [
                    "ssm:GetParameters",
                    "ssm:GetParameter"
                ],
                "Resource": "${aws_ssm_parameter.secret.arn}"
            }
        ]
    }
  EOF
}
