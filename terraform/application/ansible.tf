# HACK: Putting ansible code in s3 bucket, for production use
# Use systems manager or code deploy

resource "aws_s3_bucket" "nginx-echo-provisioner" {
    bucket = "scratchpay-nginx-echo-provisioner"
}

data "archive_file" "ansible" {
  type        = "zip"
  source_dir = "${path.module}/../../ansible"
  output_path = "ansible.zip"
}

resource "aws_iam_role_policy" "ansible-bucket-permission" {
  role = aws_iam_role.nginx_echo_iam_role.name
  policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": [
                    "s3:GetObject",
                    "s3:ListObject"
                ],
                "Resource": "${aws_s3_bucket.nginx-echo-provisioner.arn}"
            },
            {
                "Sid": "VisualEditor1",
                "Effect": "Allow",
                "Action": [
                    "s3:GetObject",
                    "s3:ListObject"
                ],
                "Resource": "${aws_s3_bucket.nginx-echo-provisioner.arn}/*"
            }
        ]
    }
  EOF
}

resource "aws_s3_bucket_object" "object" {
    bucket = aws_s3_bucket.nginx-echo-provisioner.id
    key    = "ansible.zip"
    source = "${path.module}/ansible.zip"
}
